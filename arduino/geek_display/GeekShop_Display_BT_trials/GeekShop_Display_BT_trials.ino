#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "default_font.h"

SoftwareSerial btSerial(10, 9); // RX, TX

#define NO_SLAB  1
#define NO_SLICE  1
#define NO_CHAR   8//4
#define REP_COUNT   2//4
#define CHAR_LINES   16//4

int LED_BRIGHTNESS =  400; // 0 to 1000 // 400 is optimum

//bool passcheck = true;

String dispString = "GEEKSHOP";
String dispString1 = "FAILED";
String dispString2 = "CONNECTED";
String dispString3 = "
String content = "";
String defaultPassword = "geek@123";
String currentPassword = "";
 String qwerty="";
int resetPin = 21; // digital pin to reset the password to default password


float f = 123.456f;
int cPath = 100;  // content storage address
int bPath = 2;  // 112  brightness controll address
int sPath = 4;  // 115 scrolling direction controll address , ic left or right, 0 means left to right and 1 means right to left
int spPath = 6;  //  117 speed of the scrolling address
int stylepath = 8;  //  display style address
int pwdpath = 12; // default password path
int currpath = 45; // current password path
int passlength = 90; // password length path
int passwordLength; // pasword length integer



int bVal = 72;  //  brightness value
int sVal = 82;  //  scrolling direction value , ic left or right
int spVal = 92;  //  speed of the scrolling value, it is integer value
int contLen = 0;
boolean getCont = true;
int sSpeed = 25;
int style = 0;  // 0 means static and 1 means scrolling


int latchPin = 8; //LT
int clockPin = 12; //SK
int dataPin = 11; //R1
//int dataPin2 = 11; //G1

int dispAll_IO = 45;

int en_74138 = 2;

int la_74138 = 3;
int lb_74138 = 4;


int lc_74138 = 5;
int ld_74138 = 6;

int k = 0;
int lTimes = 2;


void shiftOut(unsigned char dataOut)
{
  for (int i = 7; i >= 0  ; i--)
  {
    #if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
        // Fast Data Toggler
        PORTB &= ~(1 << (6));
        if (dataOut & (0x01 << i)) PORTB &= ~(1 << (5));
        else
          PORTB |= 1 << (5);
        PORTB |= 1 << (6);
    #endif
    #if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
        //     SLOW Toggler
        digitalWrite(clockPin, LOW);
        if (dataOut & (0x01 << i))
          digitalWrite(dataPin, LOW);
        else
          digitalWrite(dataPin, HIGH);
        digitalWrite(clockPin, HIGH);
    #endif
  }
}


void cyclePower(int l)
{

  digitalWrite(en_74138, LOW);
  digitalWrite(la_74138, l % 2 > 0);
  digitalWrite(lb_74138, l % 4 > 1);
  digitalWrite(en_74138, HIGH);
  delayMicroseconds(LED_BRIGHTNESS);
  digitalWrite(en_74138, LOW);
}




uint8_t msg_board[] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};



uint8_t msgfilled_board[] =
{
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};


void display_msg(String eightchars, int delaySec)
{
  while (delaySec--) {
    display_msg(eightchars);

  }
}


void display_msg(String eightchars)
{

  // Reduce string length to 8
  if (eightchars.length() > 8)
    eightchars = eightchars.substring(0, 8);

  // Optimize string with spaces to 8
  if (eightchars.length() < 8)
  {
    String temp = "";
    for (int i = 0; i < (8 - eightchars.length()) / 2; i++)
      temp += " ";
    temp += eightchars;
    for (int i = 0; i < (((8 - eightchars.length()) / 2) + eightchars.length() % 2); i++)
      temp += " ";
    eightchars = temp;
  }


  // Set msgBoardData here
  for (int y = 0; y < NO_CHAR; y++)
    for (int x = 0; x < CHAR_LINES; x++)
    {
      msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (eightchars.charAt(y) * CHAR_LINES) + x);
    }
  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + (l)]);
          digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
          //delay(10);
        }
      }

      cyclePower(l);
    }
  }
}


void display_msgfilled()
{

  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          shiftOut(msgfilled_board[m * CHAR_LINES + ((4 * 3) + l)]);
          shiftOut(msgfilled_board[m * CHAR_LINES + ((4 * 2) + l)]);
          shiftOut(msgfilled_board[m * CHAR_LINES + ((4 * 1) + l)]);
          shiftOut(msgfilled_board[m * CHAR_LINES + (l)]);
          digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
          //delay(10);
        }
      }

      cyclePower(l);
    }
  }
}



void displayScroll_msg(String longString)
{

  longString = "        " + longString;

  for (int m = 0; m < longString.length(); m ++)
  {
    String shortString = "";
    if (m + 8 < longString.length())
      shortString = longString.substring(m, m + 8);
    else
      shortString = longString.substring(m);


    // Set msgBoardData here
    for (int y = 0; y < NO_CHAR; y++)
      for (int x = 0; x < CHAR_LINES; x++)
      {
        msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x);
      }
    int k = sSpeed;
    while (k--)
    {
      if (btSerial.available())
      {
        getBluetoothdata();

      }
      digitalWrite(en_74138, LOW);//Turn off display
      for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
      {
        for (int l = 3; l >= 0 ; l--)
        {
          for (int m = 0; m < NO_CHAR; m++)
          {
            shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
            shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
            shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
            shiftOut(msg_board[m * CHAR_LINES + (l)]);
            digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
          }
          cyclePower(l);
        }
      }
    }

  }
}

void display_flush()
{
  digitalWrite(en_74138, LOW);
  for (int l = 0; l < NO_SLAB * NO_SLICE ; l++)
  {
    shiftOut(0);
    digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
  }
  digitalWrite(en_74138, HIGH);

}


void display_flood()
{
  int delaySec1 = 300;
  while (delaySec1--) {
    display_msgfilled();
  }
}

void init_matrixDisplay()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(en_74138, OUTPUT);
  pinMode(la_74138, OUTPUT);
  pinMode(lb_74138, OUTPUT);
  pinMode(lc_74138, OUTPUT);
  pinMode(ld_74138, OUTPUT);

  pinMode(dispAll_IO, INPUT);

  digitalWrite(lc_74138, LOW);
  digitalWrite(ld_74138, LOW);
  digitalWrite(en_74138, LOW);
  display_flush();
}

boolean testLED = false;

void setup()
{
  Serial.begin(9600); Serial.setTimeout(50);
  btSerial.begin(9600); btSerial.setTimeout(50);
  init_matrixDisplay();
  display_flush();

  pinMode(resetPin, INPUT_PULLUP);
  
  for (int r = 0; r < 300; r++)
  {
    display_msg(dispString);
  }
  contLen = dispString.length();
  if (EEPROM.read(cPath) < 3)
  {
    EEPROM.write(cPath, contLen);
    for (int i = 0; i < dispString.length(); i++)
    {
      int count = cPath + 1 + i;
      EEPROM.write(count, dispString.charAt(i));
    }
    EEPROM.write(spPath, 25);
    EEPROM.write(bPath, 80);
    EEPROM.write(sPath, 1);
    EEPROM.write(stylepath, 0);
  }
  for(int i=0;i<defaultPassword.length();i++)
  {
     int count = pwdpath + 1 + i;
     EEPROM.write(count, defaultPassword.charAt(i));
  }


  Serial.println("current pass befor init: "+ currentPassword);

  int checkLength = EEPROM.read(passlength);
              
  Serial.println("Password length after eeprom read is: "+checkLength);

    for(int i=0;i<checkLength;i++)
      {
         int count = currpath + 1 + i;
         qwerty += char(EEPROM.read(count));
      }
      Serial.println(" Current password after eeprom read in setup option is: "+qwerty);
      currentPassword = qwerty;
   
  if(currentPassword == "")
  {
      currentPassword = defaultPassword ;
      passwordLength = currentPassword.length();
              
      Serial.println("Password length is :"+currentPassword.length());
      
      EEPROM.write(passlength,passwordLength);
              
      int checkLength = EEPROM.read(passlength);

       for(int i=0;i<checkLength;i++)
        {
           int count = currpath + 1 + i;
           EEPROM.write(count, currentPassword.charAt(i));
        }
         Serial.println("default data written");
  }

    Serial.println("current password at the end of setup is : "+ currentPassword);
    String qwer = "";
    for(int i=0;i<checkLength;i++)
      {
         int count = currpath + 1 + i;
         qwer += char(EEPROM.read(count));
      }
      Serial.println(" Current password after eeprom read default password in setup option is: "+qwer);

  
 
   
}


void loop() {
  int sensorVal = digitalRead(resetPin);
  Serial.println(sensorVal);

  if(sensorVal == 0)
  {
    clearEEpromData();
//      for (int i = 100 ; i < EEPROM.length() ; i++)
//      {
//        EEPROM.write(i, 0);
//      }
     Serial.println("all data cleared");
  }

  
  // initialize all the value from eeprom before start.
  if (getCont == true) {
    getCont = false;
    contLen = EEPROM.read(cPath);
    content = "";
    for (int j = 0; j < contLen; j++)
    {
      int coV = cPath + 1 + j;
      content += char(EEPROM.read(coV));
    }
    dispString = content;
    sSpeed = EEPROM.read(spPath); // adding new value to scrolling speed from bluetooth.
    sSpeed = 200 - sSpeed * 2;
    LED_BRIGHTNESS = EEPROM.read(bPath); // adding new value to scrolling speed from bluetooth.
    LED_BRIGHTNESS = LED_BRIGHTNESS * 15;
    style = EEPROM.read(stylepath);
  }

  // read the data from bluetooth
  if (btSerial.available())
  {
     getBluetoothdata();
  }
  if (testLED) {
    display_flood();
  }
  else {
    if (style == 0)
    {
      display_msg(dispString, 100);
    }
    else
    {
      displayScroll_msg(dispString);
    }
  }
}

void getBluetoothdata() {
  String dataRec = btSerial.readString();
  Serial.println(dataRec);
  String contCheck = dataRec.substring(0, dataRec.indexOf('~'));
  if (contCheck.equals("Style"))
  {
    style = dataRec.substring(dataRec.indexOf('~') + 1).toInt();
    setDataAttribute(style, stylepath);   // here we have decide display style, ic static or scrolling
  }

  if (contCheck.equals("old"))      // Condition becomes true whenever reset option is given
  {
         
    
            String oldPassword = (dataRec.substring((dataRec.indexOf('~')+1), dataRec.indexOf(',')));
            Serial.println("old pass: "+ oldPassword);
        
            String newPassword = (dataRec.substring((dataRec.indexOf(',')+5), dataRec.length()));
            Serial.println("new pass: "+ newPassword);
        
            if (oldPassword == currentPassword)
            {
              btSerial.print("true"); 
              
              currentPassword = newPassword;
              
              passwordLength = currentPassword.length();
              
              Serial.println("Password length is :"+currentPassword.length());
              
              EEPROM.write(passlength,passwordLength);
              
              int checkLength = EEPROM.read(passlength);
              
              Serial.println("Password length after eeprom read is: "+checkLength);
              
              for(int i=0;i<checkLength;i++)
                {
                   int count = currpath + 1 + i;
                   EEPROM.write(count, currentPassword.charAt(i));
                }
              qwerty = "";
              for(int i=0;i<checkLength;i++)
                {
                   int count = currpath + 1 + i;
                   qwerty += char(EEPROM.read(count));
                }
                Serial.println(" Current password after eeprom read in reset password option is: "+qwerty);

                             
            }
            else
            {
              btSerial.print("false");
            }
  }


  
  
  if (contCheck.equals("password"))  // Condition becomes true whenever user opens the app
  {
                  String currentPasswordd;
                  String key = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.length()));
                  Serial.println(key);
                  Serial.println("current password is: "+currentPassword);

                  int checkLength = EEPROM.read(passlength);
              
                  Serial.println("Password length after eeprom read in password check is: "+checkLength);
                  checkLength = EEPROM.read(passlength);
                  qwerty = "";

//String qwer = "";
//for(int i=0;i<currentPassword.length();i++)
//  {
//     int count = currpath + 1 + i;
//     qwer += char(EEPROM.read(count));
//  }
                  
                  for(int i=0;i<checkLength;i++)
                    {
                       int count = currpath + 1 + i;
                       qwerty += char(EEPROM.read(count));
                    }
                  Serial.println("current password after eeprom read in first time app open is: "+qwerty);
                  if(key!= qwerty)
                  {
                     btSerial.print("false");
                     for (int r = 0; r < 300; r++)
                    {
                        display_msg(dispString1);
                    }
                  }
                  else
                  {
                    btSerial.print("true");
                    for (int r = 0; r < 300; r++)
                    {
                        display_msg(dispString2);
                    }

                  }
                 

  }


if (contCheck.equals("Content"))
  {
    changeContent(dataRec.substring(dataRec.indexOf('~') + 1), cPath); // here cPath for content address, it will change as per the bluetooth data
  }


  
  if (contCheck.equals("Brightness"))
  {
    LED_BRIGHTNESS = dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt() * 15;
    setDataAttribute(dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt(), bPath);
  }
  if (contCheck.equals("ON"))
  {
    testLEDdisplay();
  }
  if (contCheck.equals("OFF"))
  {
    testLED = false;
  }
  if (contCheck.equals("SSpeed"))
  {
    sSpeed = 200 - dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt() * 2;
    setDataAttribute(dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt(), spPath);
  }
//  btSerial.write("a");
//  Serial.println("a");
}

void testLEDdisplay() {
  testLED = true;
  display_msg("TEST LED", 100);
  display_flood();
}
void changeContent(String data, int pathLen)
{
  getCont = true;
  dispString = data;
  int cLen = data.length();
  EEPROM.write(pathLen, cLen);
  for (int i = 0; i < data.length(); i++)
  {
    int count = pathLen + 1 + i;
    EEPROM.write(count, data.charAt(i));
  }
}


void setDataAttribute(int data, int pathLen)
{
  EEPROM.write(pathLen, data);
}

void clearEEpromData()
{
  for (int i = 0 ; i < EEPROM.length() ; i++)
  {
    EEPROM.write(i, 0);
  }
}
